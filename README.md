# Basis Template LaTeX untuk laporan PKL (Praktik Kerja Lapangan)

Repository ini bertindak sebagai 'basis' template yang nantinya bisa
diubah sedemikian rupa untuk menyesuaikan template universitas tertentu.

## Daftar Template

- [Fakultas Ilmu Komputer (FILKOM) - Universitas Brawijaya](https://gitlab.com/template-tugasakhir-latex/template-pkl-filkom) (unofficial)

## Panduan

Silahkan merujuk pada [docs](docs/)

## Mengapa LaTeX ?

Sudah banyak sekali sumber yang dapat menjawab pertanyaan ini secara
detail. Cukup lakukan pencarian dengan kata kunci "why
latex". Beberapa diantaranya yang menjadi alasan kuat kami:

- Tidak ada lagi dokumen dengan nama 'jadi1', 'jadibanget',
  'jadifinal'. Dengan fasilitas yang diberikan *revision control*.
- Semua kelebihan yang didapat dari *revision control*. (tag, branch)
- Kemudahan untuk membuat *branch* baru ketika terdapat revisi. Oleh
  karena itu, mengatur dokumen dengan perubahan mana yang harus
  digunakan, mudah dilakukan.
- Kemudahan sitasi. Tidak perlu lagi menulis semua sitasi secara
  manual. Terima kasih kepada [Biblatex](https://github.com/plk/biblatex)
- Menulis LaTeX dengan *text editor* favorit memberikan banyak
  manfaat. Seperti kecepatan *editing*, *completion*, *revision
  control integration* dan banyak lainnya. Terutama *keybinding* yang
  tidak tersedia di luar *text editor* favorit kita.
- Lebih fokus pada konten dan tidak terdistraksi dengan gambar
  di sana sini.
- Tidak ada [vendor lock-in](https://en.wikipedia.org/wiki/Vendor_lock-in)

## Hasil Ekspor

Hasil ekspor : [laporan-pkl.pdf](/uploads/eacd588cbbdad4da910a7c3deaf07554/laporan-pkl.pdf)

*PS: ekspor ini bisa jadi tidak sesuai dengan perubahan terbaru*

## Laporan yang menggunakan template-pkl-base

Silahkan melihat wiki [Laporan yang dibuat dari template pkl base](https://gitlab.com/template-tugasakhir-latex/template-pkl-base/wikis/Laporan-yang-dibuat-dari-template-pkl-base)

## Bug yang dikenal (*known bug*)

Nomer ('No') tampil dua kali pada beberapa kolom kode. Bug tidak
bersifat idempoten sehingga sulit dilacak (kadang kala muncul kadang
kala tidak). Kami sedang memperbaikinya. Untuk sementara waktu, solusi
sementara kami adalah dengan menghapusnya secara manual menggunakan
fitur highlight berwarna putih dari pdf tool.


## Pengembang

Lihat [AUTHORS](AUTHORS)

